package java_sql;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Vector;

import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import java.awt.Button;
import javax.swing.ButtonGroup;
import com.toedter.calendar.JDateChooser;

import net.proteanit.sql.DbUtils;

import javax.swing.JTree;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;
import javax.swing.UIManager;
import java.awt.Color;
public class Read_Find extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField tid;
	private JTextField tname;
	private JTextField tage;
String gender;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Read_Find frame = new Read_Find();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
Connection con;
PreparedStatement pt;
ResultSet rs;
ResultSetMetaData rd;
DefaultTableModel model;
private final ButtonGroup buttonGroup = new ButtonGroup();
private JTextField tgender;
private JTextField tcontact;
private JTextField tsalary;
private JTable table;
	
	public void Connect() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			String url="jdbc:mysql://localhost:3306/praveen";
			String userName="root";
			String password="system";
			con=DriverManager.getConnection(url,userName,password);
			}
		catch(Exception e){
			
		}
	}
	
	
			
		

	/**
	 * Create the frame.
	 */
	public Read_Find() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 682, 441);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Employee Details");
		lblNewLabel.setFont(new Font("Calibri", Font.BOLD, 22));
		lblNewLabel.setBounds(244, 11, 169, 23);
		contentPane.add(lblNewLabel);
		
		JLabel Id = new JLabel("Id");
		Id.setFont(new Font("Tahoma", Font.BOLD, 15));
		Id.setBounds(87, 70, 32, 14);
		contentPane.add(Id);
		
		tid = new JTextField();
		tid.setBounds(221, 69, 86, 20);
		contentPane.add(tid);
		tid.setColumns(10);
		
		JLabel Name = new JLabel("Name");
		Name.setFont(new Font("Tahoma", Font.BOLD, 15));
		Name.setBounds(87, 129, 46, 14);
		contentPane.add(Name);
		
		JLabel Age = new JLabel("Age");
		Age.setFont(new Font("Tahoma", Font.BOLD, 15));
		Age.setBounds(394, 66, 32, 23);
		contentPane.add(Age);
		
		tname = new JTextField();
		tname.setBounds(221, 126, 86, 20);
		contentPane.add(tname);
		tname.setColumns(10);
		
		tage = new JTextField();
		tage.setBounds(509, 69, 86, 20);
		contentPane.add(tage);
		tage.setColumns(10);
		
		JButton find = new JButton("Search");
		find.setFont(new Font("Tahoma", Font.BOLD, 13));
		find.setBackground(new Color(50, 205, 50));
		find.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
					pt=con.prepareStatement("select * from empdata where id=?");
					pt.setInt(1, Integer.parseInt(tid.getText()));
					rs=pt.executeQuery();
					table.setModel(DbUtils.resultSetToTableModel(rs));
					if(rs.next()==true) {
						String pa=rs.getString("name");
						String pri=rs.getString("age");
						String pro=rs.getString("contact");
						String pp=rs.getString("gender");
						String sa=rs.getString("salary");
						tname.setText(pa);
						tage.setText(pri);
						tcontact.setText(pro);
						tgender.setText(pp);
						tsalary.setText(sa);
					}
//					else {
//						tname.setText("not found");
//						tage.setText("not found");
//						tcontact.setText("not found");
//						tgender.setText("not found");
//						tsalary.setText("not found");
//					}
					
					}
				catch(Exception ae){
					
				}
			}
		});
		find.setBounds(43, 36, 89, 23);
		contentPane.add(find);
		
		JButton Add = new JButton("Add");
		Add.setFont(new Font("Tahoma", Font.BOLD, 13));
		Add.setBackground(new Color(147, 112, 219));
		Add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id=Integer.parseInt(tid.getText());
				String name=tname.getText();
				int age=Integer.parseInt(tage.getText());
				String contact=tcontact.getText();
				String gender=tgender.getText();
				int salary=Integer.parseInt(tsalary.getText());
				try {
					pt=con.prepareStatement("insert into empdata(id,name,age,contact,gender,salary)values(?,?,?,?,?,?)");
					pt.setInt(1, id);
					pt.setString(2, name);
					pt.setInt(3, age);
					pt.setString(4, contact);
					pt.setString(5, gender);
					pt.setInt(6, salary);
					
					pt.executeUpdate();
					JOptionPane.showMessageDialog(null, "Successfully Added");
					table_load();
				} 
				catch (SQLException e1) {
					
					e1.printStackTrace();
				}
			}
		});
		Add.setBounds(68, 249, 89, 23);
		contentPane.add(Add);
		
		JButton Update = new JButton("Update");
		Update.setFont(new Font("Tahoma", Font.BOLD, 13));
		Update.setBackground(new Color(218, 165, 32));
		Update.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id=Integer.parseInt(tid.getText());
				String name=tname.getText();
				int age=Integer.parseInt(tage.getText());
				String contact=tcontact.getText();
				String gender=tgender.getText();
				int salary=Integer.parseInt(tsalary.getText());
				try {
					pt=con.prepareStatement("update empdata set name=?, age=?, contact=?, gender=?, salary=?  where id=?");
					
					pt.setString(1, name);
					pt.setInt(2, age);
					pt.setString(3, contact);
					pt.setString(4, gender);
					pt.setInt(5, salary);
					pt.setInt(6, id);
					
					pt.executeUpdate();
					JOptionPane.showMessageDialog(null, "Successfully Updated");
					table_load();
				} 
				catch (SQLException e1) {
					
					e1.printStackTrace();
				}
			}
		});
		Update.setBounds(365, 249, 89, 23);
		contentPane.add(Update);
		
		JButton Delete = new JButton("Delete");
		Delete.setFont(new Font("Tahoma", Font.BOLD, 13));
		Delete.setBackground(new Color(255, 0, 0));
		Delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id=Integer.parseInt(tid.getText());
				try {
					pt=con.prepareStatement("delete from empdata where id=?");
					
					
					pt.setInt(1, id);
					pt.executeUpdate();
					JOptionPane.showMessageDialog(null, "Successfully Deleted");
					table_load();
				} 
				catch (SQLException e1) {
					
					e1.printStackTrace();
				}
				
			}
		});
		Delete.setBounds(506, 249, 89, 23);
		contentPane.add(Delete);
		
		JButton Clear = new JButton("Clear");
		Clear.setFont(new Font("Tahoma", Font.BOLD, 13));
		Clear.setBackground(new Color(102, 205, 170));
		Clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tid.setText("");
				tname.setText("");
				tage.setText("");
				tcontact.setText("");
				tgender.setText("");
				tsalary.setText("");
				tid.requestFocus();
				
			}
		});
		Clear.setBounds(218, 249, 89, 23);
		contentPane.add(Clear);
		
		JLabel Gender = new JLabel("Gender");
		Gender.setFont(new Font("Tahoma", Font.BOLD, 15));
		Gender.setBounds(394, 129, 58, 14);
		contentPane.add(Gender);
		
		tgender = new JTextField();
		tgender.setBounds(509, 128, 86, 20);
		contentPane.add(tgender);
		tgender.setColumns(10);
		
		JLabel contact = new JLabel("Contact");
		contact.setFont(new Font("Tahoma", Font.BOLD, 15));
		contact.setBounds(87, 182, 65, 14);
		contentPane.add(contact);
		
		tcontact = new JTextField();
		tcontact.setBounds(221, 181, 86, 20);
		contentPane.add(tcontact);
		tcontact.setColumns(10);
		
		JLabel salary = new JLabel("Salary");
		salary.setFont(new Font("Tahoma", Font.BOLD, 15));
		salary.setBounds(394, 178, 65, 23);
		contentPane.add(salary);
		
		tsalary = new JTextField();
		tsalary.setBounds(509, 181, 86, 20);
		contentPane.add(tsalary);
		tsalary.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(57, 305, 571, 69);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setBackground(new Color(240, 255, 240));
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {

		         try{ 
		        int i= table.getSelectedRow();
		        DefaultTableModel model=(DefaultTableModel) table.getModel();
		       

			 tid.setText(model.getValueAt(i,0).toString());
		       	tname.setText(model.getValueAt(i,1).toString());
		         tage.setText(model.getValueAt(i,2).toString());
		       	 tcontact.setText(model.getValueAt(i,3).toString());
		         tgender.setText(model.getValueAt(i,4).toString());
		         tsalary.setText(model.getValueAt(i,5).toString());
		}
		  catch(Exception ex){
			  ex.printStackTrace();
		   }
		        
			}
		});
		scrollPane.setViewportView(table);
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
			},
			new String[] {
				"Id", "Name", "Age", "Contact", "Gender", "Salary"
			}
		));
		Connect();
		table_load();
	}
	
	public void table_load() {
		 try{
	          	 pt= con.prepareStatement("select * from empdata");
	     ResultSet rs=pt.executeQuery();
	     table.setModel(DbUtils.resultSetToTableModel(rs));
	      }
	      catch(SQLException e){
	          e.printStackTrace();
	      }
	}
	
}
